const API_URL = process.env.WP_API_URL

console.log(API_URL)

const fetchAPI = async (query, { variables } = {}) => {
  const headers = { 'Content-Type': 'application/json' }

  const res = await fetch(API_URL, {
    method: 'POST',
    headers,
    body: JSON.stringify({ query, variables })
  })

  console.log(`res: ${JSON.stringify(res)}`)

  const json = await res.json()

  if (json.errors) {
    console.log(json.errors)
    console.log('error details', query, variables)
    throw new Error('Failed to fetch API')
  }

  return json.data
}

export const getAllPosts = async (preview) => {
  const data = await fetchAPI(
    `
    query AllPosts {
      posts(first: 20, where: { orderby: { field: DATE, order: DESC}}) {
          edges {
            node {
              id
              title
              categories {
                nodes {
                  name
                }
              }
              date
              slug
            }
          }
        }
      }
    `
  )

  return data?.posts
}

export const getAllPostSlugs = async () => {
  const data = await fetchAPI(
    `
    query GetPostSlugs {
      posts(first: 10000) {
          edges {
            node {
              slug
            }
          }
        }
      }
    `
  )

  return data?.posts
}

export const getPost = async (slug) => {
  const data = await fetchAPI(
    `
    fragment PostFields on Post {
      title
      excerpt
      slug
      date
    }

    query PostBySlug($id: ID!, $idType: PostIdType!) {
      post(id: $id, idType: $idType) {
        ...PostFields
        content
      }
    }
    `,
    {
      variables: {
        id: slug,
        idType: 'SLUG'
      }
    }
  )

  return data
}
