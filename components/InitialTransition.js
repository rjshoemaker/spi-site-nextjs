import { motion } from 'framer-motion'
import styles from '../styles/Initial.module.css'

const blackBox = {
  initial: {
    height: '100vh',
    bottom: 0
  },
  animate: {
    height: 0,
    transition: {
      duration: 1.5,
      ease: [.45, 0, .55, 1]
    }
  }
}

const InitialTransition = () => {
  return (
    <div className={styles.transitionWrapper}>
      <motion.div
        className={styles.transition}
        initial='initial'
        animate='animate'
        variants={blackBox}
      />
    </div>
  )
}

export default InitialTransition