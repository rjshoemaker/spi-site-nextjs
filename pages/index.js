import Head from 'next/head'
import Link from 'next/link'
import Image from 'next/image'
import Tilt from 'react-tilt'
import styles from '../styles/Index.module.css'
import { motion } from 'framer-motion'
// import homeImage from '../images/home-image.png'

export default function Home() {
  const headerText = 'spi group'
  const subHeaderText = 'strategy. planning. implementation'
  const title = {
    hidden: {
      opacity: 1
    },
    visible: {
      opacity: 1,
      transition: {
        delay: 0.5,
        staggerChildren: 0.08
      }
    }
  }

  const titleLetter = {
    hidden: {
      opacity: 0
    },
    visible: {
      opacity: 1
    }
  }

  const subTitle = {
    hidden: {
      opacity: 1
    },
    visible: {
      opacity: 1,
      transition: {
        delay: 1.5,
        staggerChildren: 0.03,
        staggerDirection: -1,
        delayChildren: 0.5
      }
    }
  }

  const subtitleLetter = {
    hidden: { opacity: 0 },
    visible: { opacity: 1, transition: { duration: 1 } }
  }

  const imageWrapper = {
    hidden: {
      opacity: 0,
      y: -800
    },
    visible: {
      y: 0,
      opacity: 1,
      transition: {
        type: 'spring',
        bounce: 0.3,
        duration: 3.5,
        velocity: 0.5
      }
    },
    exit: {
      y: -800
    }
  }

  const enterButton = {
    hidden: {
      y: 300
    },
    visible: {
      y: 0,
      transition: {
        type: 'spring',
        delay: 3,
        duration: 1.8
      }
    }
  }
  return (
    <motion.div className={`bg-dark ${styles.homeContainer}`}>
      <div className={styles.homeContentContainer}>
        <div className={`flex-1 ${styles.leftSection}`}>
          <motion.h1
            className={`text-light ${styles.homeHeader}`}
            variants={title}
            initial='hidden'
            animate='visible'
            layoutId='title'
          >
            {headerText.split('').map((char, index) => {
              return (
                <motion.span key={`char-${index}`} variants={titleLetter}>
                  {char}
                </motion.span>
              )
            })}
          </motion.h1>
          <motion.h2
            className={`text-red ${styles.homeSubHeader}`}
            variants={subTitle}
            initial='hidden'
            animate='visible'
            layoutId='subtitle'
          >
            {subHeaderText.split('').map((char, index) => {
              return (
                <motion.span key={`char-${index}`} variants={subtitleLetter}>
                  {char}
                </motion.span>
              )
            })}
          </motion.h2>
        </div>
        <div className={`flex-1 ${styles.rightSection}`}>
          <motion.div
            variants={imageWrapper}
            initial='hidden'
            animate='visible'
          >
            <Image src='/images/home-image.png' width={400} height={400} />
          </motion.div>
        </div>
      </div>
      <div className={styles.bottomSection}>
        <Link href='/home'>
          <motion.div
            variants={enterButton}
            initial='hidden'
            animate='visible'
            exit='exit'
          >
            <Tilt
              className={styles.enterButton}
              options={{ max: 50, reverse: true, perspective: 500 }}
            >
              Enter
            </Tilt>
          </motion.div>
        </Link>
      </div>
    </motion.div>
  )
}
