import Link from 'next/link'
import { motion } from 'framer-motion'
import styles from '../styles/Home.module.css'

const Home = () => {
  return (
    <div className={`bg-dark ${styles.homeContainer}`}>
      <div className={styles.homeNavWrapper}>
        <motion.h1
          className={`text-light ${styles.homeNavTitle}`}
          layoutId='title'
        >
          spi group
        </motion.h1>
        <motion.h2
          className={`text-red ${styles.homeNavSubtitle}`}
          layoutId='subtitle'
        >
          strategy. planning. implementation.
        </motion.h2>
      </div>
      <Link href='/blog'>
        <a className={styles.blogLink}>Blog</a>
      </Link>
    </div>
  )
}

export default Home
