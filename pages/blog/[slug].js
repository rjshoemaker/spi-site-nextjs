import { useRouter } from 'next/router'
import Head from 'next/head'
import Link from 'next/link'
import { getAllPostSlugs, getPost } from '../../lib/api'
import styles from '../../styles/Post.module.css'
import { motion } from 'framer-motion'

const Post = ({postData}) => {
  const router = useRouter()

  if (!router.isFallback && !postData?.slug) {
    return <p>Looks like an error.</p>
  }

  const formatDate = date => {
    const newDate = new Date(date)

    return `${newDate.getMonth()}/${newDate.getDate()}/${newDate.getFullYear()}`
  }

  return (
    <div className='container'>
      <Head>
        <title>{`Spi Group | ${postData.title}`}</title>
      </Head>

      <div>
        {router.isFallback ? (
          <h2>Loading...</h2>
        ) : (
          <motion.div
            initial={{x: -2000, rotate: 90}}
            animate={{x: 0, rotate: 0 }}
            exit={{x: -2000, rotate: -90}}
            transition={{type: 'spring', duration: 0.8}}
          >
            <div style={{marginBottom: '50px'}}>
              <h1 className='header'>{postData.title}</h1>
              <h3>{formatDate(postData.date)}</h3>
              <div className={styles.postContent}>{postData.content.replace(/<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&gt;/g, ' ')}</div>
            </div>
            <Link href='/blog'>
              <a className={styles.blogLink}>Back to articles</a>
            </Link>
          </motion.div>
        )}
      </div>
    </div>
  )
}

export default Post

export const getStaticPaths = async () => {
  const allPosts = await getAllPostSlugs()

  return {
    paths: allPosts.edges.map(({ node }) => `/blog/${node.slug}`) || [],
    fallback: 'blocking'
  }
}

export const getStaticProps = async ({ params }) => {
  const data = await getPost(params.slug)

  return {
    props: {
      postData: data.post
    }
  }
}