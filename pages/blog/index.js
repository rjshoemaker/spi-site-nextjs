import Head from 'next/head'
import Link from 'next/link'
import { motion } from 'framer-motion'
import { getAllPosts } from '../../lib/api'
import styles from '../../styles/Blog.module.css'

const Blog = ({ allPosts: { edges }}) => (
  <motion.div
    initial={{x: -2000, rotate: -90}}
    animate={{x: 0, rotate: 0 }}
    exit={{x: 2000, rotate: 90}}
    transition={{type: 'spring', duration: 0.8}}
  >
    <Head>
      <title>Spi Group | Blog</title>
    </Head>
    <div className='container'>
      <div className={styles.flexWrapper}>
        <h1 className={`header ${styles.blogHeader}`}>Blog</h1>
        <Link className={styles.homeLink} href='/'>
          <a className={styles.homeLinkA}>Back Home</a>
        </Link>
      </div>
      <div className={styles.postsWrapper}>
        {edges.map(({ node }) => (
          <div className={styles.postItem} key={node.id}>
            <h2>{node.title}</h2>
            <Link href={`./blog/${node.slug}`}>
              <a>Read more</a>
            </Link>
          </div>
        ))}
      </div>
    </div>
  </motion.div>
)

export default Blog

export const getStaticProps = async () => {
  const allPosts = await getAllPosts()

  return {
    props: {
      allPosts
    },
    revalidate: 10
  }
}